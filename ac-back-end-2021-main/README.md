# Welcome to AC Back-end!
În primul și primul rând am vrea să începem prin a vă mulțumi pentru faptul că sunteți astăzi aici și să vă felicităm pentru toată munca pe care ați depus-o până acum în cadrul întregului proces de recrutare. **Well done!**

![enter image description here](https://media.giphy.com/media/d31w24psGYeekCZy/giphy.gif)

Acum, pentru ceea ce urmează, vă îndemnăm să vă încărcați cu și mai multă energie și bună dispoziție și vă promitem că fiecare moment și fiecare minut dedicat **SiSC**-ului va merita din plin.
**Și pentru că suntem IT și ne place eficiența, să ne focusăm asupra acestui aspect acum.**

![](https://media.giphy.com/media/1nR6fu93A17vWZbO9c/giphy.gif)

Pentru început, după cum probabil ați realizat, **Git** este o unealtă de bază pentru noi. Aici ajung toate proiectele noastre și de aici pleacă mai departe toate creațiile către publicul larg.
Pentru că ne oferă posibilitatea să menținem și lucrăm întotdeauna pe ultima versiune a fișierelor, acesta aduce un avantaj enorm unei **echipe** care colaborează constant, deci o să vă rugăm și pe voi să îi acordați la fel de multă importanță și să fiți atenți în momentul în care vom explica funcționalitățile de bază.


![enter image description here](https://media.giphy.com/media/yDYAHbqe5DfyM/giphy.gif)
  
Puțin mai jos vom lista și **necesarele + materialele** pe care vi le-am pus la dispoziție acum câteva zile, în cazul în care va fi nevoie să le accesați ceva mai rapid:
*Tutoriale:*
 - [**JavaScript basics**](https://www.codecademy.com/learn/introduction-to-javascript)
 - **[SQL basics](https://www.edureka.co/blog/sql-basics/)**
 - **[NodeJS basics](https://www.youtube.com/watch?v=U8XF6AFGqlc)**
  
 *Programe de instalat:*
  
 - **[Xampp](https://www.apachefriends.org/ro/download.html)**
 ```Precizare: nu îl instalați în C:\Program files```
 - **[Visual Studio Code](https://code.visualstudio.com/)**
 - **[NodeJS](https://nodejs.org/en/)**
 - **[Git](https://git-scm.com/downloads)**

![enter image description here](https://media.giphy.com/media/TOWeGr70V2R1K/giphy.gif)

# Acum, să trecem la treabă!
## **Ce veți avea de făcut mai exact?**
Prima dată va trebui să facem rost de materialele cu care vom lucra. Pentru a face asta deschidem:
-   **Linia de comandă** - dacă avem Ubuntu
-   **[Git bash](https://git-scm.com/download/win)** - dacă avem Windows
Vom crea un folder, iar aici vom face practic **o clonă** la tot ceea ce v-am pregătit, folosind comanda:

`git clone REPO_HTTPS_ADDRESS`

În cazul nostru:

`git clone https://gitlab.com/itsisc/ac-back-end-2021.git`

Acum, dacă navigăm în folderul de mai sus...

![enter image description here](https://media.giphy.com/media/3rlkI1QFJWRUsdRtha/giphy.gif)

...vom găsi un sub-folder nou, cu toate fișierele din acest **repository**.
Mai mult sau mai puțin, acolo regăsim **un formular**, în care există anumite **cerințe**, de care va trebui să ne ocupăm astăzi:
# Enigma I: 
![enter image description here](https://media.giphy.com/media/42wQXwITfQbDGKqUP7/giphy.gif)

Acest formular este puțin mai special deoarece o facultate din ASE a avut o mică dispută cu divizia IT, iar aceștia au făcut o mică glumă stricându-le formularul de înscriere la facultate. Voi, SiSCotii, va trebui să-i ajutați să repare formularul.
## Ce veți avea de făcut?

![enter image description here](https://media.giphy.com/media/o0vwzuFwCGAFO/giphy.gif)

- Accesați folderul tocmai clonat, apoi accesați **formular1**
 - În fereastra de bash rulați comanda:
 `code.`
 O să se deschidă Visual Studio Code automat unde veți găsi formularul cu probleme.
  
 - Rulați modulele **Apache** și **MySQL** din XAMPP Control Panel
 - Se crează baza de date **siscit_back_end** cu **Utf8_general_ci la Collation**, nu vă speriați este extrem de simplu și vă vom arăta cum să faceți asta ;)
 - Deschidem terminalul în Visual Studio Code și rulăm următoarele comenzi: 
 
	`cd ./back-end`
  
	`npm install` 
  
	 `npm install -g nodemon`
- Pentru a porni formularul folosiți comanda: `nodemon` sau `npm start`
- Din Visual Studio Code, accesați fișierul **server.js** și analizați în terminal erorile strecurate de ștrengarii diviziei IT astfel încât lumea să se poată înscrie la facultate.
- Atunci când în terminal nu se va mai afla nicio eroare, accesați în browserul vostru **localhost:8081**
- În momentul în care datele clienților vor fi introduse fără erori în tabela **studenti_inscrisi** din baza de date **siscit_back_end**, Facultatea va fi recunoscătoare.
> Pentru testare încercați să aveți deschise în paralel formularul din browser și terminalul din Studio Code, iar de fiecare dată când faceți modificări să îl reîncărcați. De asemenea, după modificări, nu uitați să salvați! **Ctrl + S**
> 
> În plus, pentru a nu pierde timpul, puteți prioritiza funcționalitatea, nu validitatea datelor (ex. adresa **ion@aaa.com** e la fel de bună precum **ionut.popescu@gmail.com** sau **0711222333** e la fel de ok ca **numărul vostru real de telefon**).

**Ai îndeplinit cu succes toți pașii?**

![enter image description here](https://media.giphy.com/media/lvOnlEYunAwOkHjgmU/giphy.gif)

**Felicitări!** Atunci hai să îi anunțăm și pe ceilalți că problema a fost rezolvată și să urcăm codul sursă undeva... Evident, pe Git.
-   Mergem pe profilul nostru de **Gitlab** și dăm click pe butonul mare și verde cu **New Project**.
  
 -   Numele proiectului va fi **ac back 2021**
 -   Nivelul de vizibilitate va fi **Public**
 -   Și vom bifa căsuța pentru **README.md**, pentru a-l clona mai ușor pe local
 -   *Opțional, puteți adăuga o descriere scurtă și amuzantă
  
> Important de menționat că ori de câte ori veți face modificări și veți vrea să le urcați pe Git va trebui să urmați aceiași pași. Deci atenție, pentru că nu e nimic dificil și merită din plin.
-   Pentru a verifica dacă există diferențe între repository și folderul local rulăm:
  
 
 `git status `
 
  
-   Pentru a adăuga un fișier modificat din listă avem:
  

 `git add FILE_NAME`

  
-   În caz că vrem să le adăugăm pe **toate**:
  
 
 `git add .`
 
  
-   Acum putem rula din nou **git status**, să vedem ce fișiere au fost adăugate cu succes.
  
-   Iar pentru ca toate modificările să aibă sens și toți ceilalți colaboratori să înțeleagă care a fost scopul update-ului, trebuie să oferim și un mesaj specific:
  

 `git commit -m "YOUR_MESSAGE_HERE"`
  
 `ex. -->  git commit -m "Fixed ERRORS"`

  
-   În final vom folosi o comanda ce va trimite toată informația către proiectul pe care tocmai ce l-ați creat. Sună cam așa:
  

 `git push`
 
![enter image description here](https://media.giphy.com/media/26DMUXQypGJaHhTsQ/giphy.gif)

**Link-ul repository-ului creat de voi îl puteți trimite la următoarele adrese de e-mail:**

`mihneacatana@gmail.com - Mihnea Catana, Director departament IT.`

`alex.dita2207@gmail.com -  Diță Alexandru, Lead divizie Back-End.`


Felicitări, ai terminat prima parte din mister!  

**A doua parte din mister vine în curând...**


